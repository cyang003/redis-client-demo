# Redis Client

## Usage

Set config env variables for Redis in .env file

```
REDIS_AUTHPASSWORD=redis
REDIS_HOST=172.29.249.179
REDIS_PORT=6379
```

```js
const RedisClient = require('./redis-client');
const redis = new RedisClient();
```

It will auto connect to Redis server while creating the RedisClient instance.

```js

  /**
   * This method should return a inventory document.
   * @param {string} storeId
   * @param {number} itemId 
   * @returns {object}
   */
  async getInventory(storeId, itemId);

  /**
   * Should update the inventoryDocument that match key storeId-itemId.
   * @param {string} storeId
   * @param {number} itemId
   * @param {object} inventoryDocument
   */
  async updateInventory(storeId, itemId, inventoryDocument);
  
  /**
   * Should return an inventoryJournal document.
   * @param {string} storeId
   * @param {number} itemId
   * @param {string} activityTime
   * @param {string} activityType
   * @returns {object}
   */
  async getInventoryJournal(storeId, itemId, activityTime, activityType);

  /**
   * Update an inventoryJournal document.
   * @param {string} storeId
   * @param {number} itemId
   * @param {string} activityTime
   * @param {string} activityType
   * @param {object} inventoryJournalDocument
   */
  async updateInventoryJournal(storeId, itemId, activityTime, activityType, inventoryJournalDocument);

  /**
   * Update activity. (Same as updateInventoryJournal function)
   * @param {object} activity
   */
  async updateActivity(activity);

  /**
   * Insert activity.
   * @param {string} storeId
   * @param {number} itemId
   * @param {object} activity
   */
  async insertActivity(storeId, itemId, activity);
  }

  /**
   * Get activities.
   * @param {string} storeId
   * @param {number} itemId
   * @param {string} [options.activityType]
   * @param {string} [options.fromActivityTime] format: '2020-02-04T17:36:06Z'
   * @param {string} [options.toActivityTime] format: '2020-02-04T17:36:06Z'
   * @returns {object[]}
   */
  async getActivities(storeId, itemId, options);

  /**
   * Get item.
   * @param {string} storeId
   * @param {number} itemId
   * @returns {object}
   */
  async getItem(storeId, itemId);
```