require('dotenv').config();
const Redis = require("ioredis");
const moment = require('moment');

class RedisClient {

  constructor() {
    const authpassword = process.env.REDIS_AUTHPASSWORD;
    const host = process.env.REDIS_HOST;
    const port = process.env.REDIS_PORT;
    const url = `redis://:${authpassword}@${host}:${port}/0`;

    this.redis = new Redis(url);
    this.log = console.log;
    // this.log = () => { };
    this.log('url:', url);
  }

  async _upsert(key, doc) {
    this.log(key);
    return new Promise(async (resolve, reject) => {
      try {
        await this.redis.set(key, JSON.stringify(doc));
        this.log('_upsert success!');
        resolve(true);
      } catch (err) {
        this.log(err);
        reject(err);
      }
    });
  }

  async _get(key) {
    return new Promise((resolve, reject) => {
      this.redis.get(key, (err, result) => {
        if (err) {
          return reject(err);
        }
        return resolve(JSON.parse(result));
      });
    })
  }

  async _del(key) {
    return this.redis.del(key);
  }

  async flushall() {
    await this.redis.flushall();
  }

  /**
   * This method should return a inventory document.
   * @param {string} storeId
   * @param {number} itemId 
   * @returns {object}
   */
  async getInventory(storeId, itemId) {
    const key = `${storeId}:${itemId}:inventory`;
    this.log('getInventory key:', key);
    const res = await this._get(key);
    if (res !== null) {
      this.log('found inventory.');
      return res;
    }

    const obj = { storeId, itemId };
    await this._upsert(key, obj);
    return obj;
  }

  /**
   * Should update the inventoryDocument that match key storeId-itemId.
   * @param {string} storeId
   * @param {number} itemId
   * @param {object} inventoryDocument
   */
  async updateInventory(storeId, itemId, inventoryDocument) {
    await this._upsert(`${storeId}:${itemId}:inventory`, inventoryDocument);
  }

  /**
   * Should return an inventoryJournal document.
   * @param {string} storeId
   * @param {number} itemId
   * @param {string} activityTime
   * @param {string} activityType
   * @returns {object}
   */
  async getInventoryJournal(storeId, itemId, activityTime, activityType) {
    const key = `${storeId}:${itemId}:transaction`;
    let docs = await this._get(key);
    if (docs == null) {
      docs = [];
    }
    this.log('activityTime:', activityTime)
    this.log('docs:', docs);

    const filteredDocs = docs.filter(d => moment(d.activityTime).isSame(moment(activityTime)) && d.activityType == activityType);
    this.log('filteredDocs:', filteredDocs);
    if (filteredDocs.length > 0) {
      this.log('found inventoryJournal.');
      return filteredDocs[0];
    }
    this.log('not found, will create a new one.')
    await this._upsert(key, [{ storeId, itemId, activityTime, activityType }]);
    return { storeId, itemId, activityTime, activityType };
  }

  /**
   * Update an inventoryJournal document.
   * @param {string} storeId
   * @param {number} itemId
   * @param {string} activityTime
   * @param {string} activityType
   * @param {object} inventoryJournalDocument
   */
  async updateInventoryJournal(storeId, itemId, activityTime, activityType, inventoryJournalDocument) {
    const key = `${storeId}:${itemId}:transaction`;
    const activity = inventoryJournalDocument;
    let docs = await this._get(key);
    if (docs == null) {
      docs = [];
    }
    this.log('docs:', docs);
    const idx = docs.findIndex(d => moment(d.activityTime).isSame(moment(activityTime)) && d.activityType == activityType);
    this.log('idx:', idx);
    if (idx == -1) {
      docs.push(activity);
    } else {
      docs[idx] = activity;
    }
    await this._upsert(key, docs);
  }

  /**
   * Update activity. (Same as updateInventoryJournal function)
   * @param {object} activity
   */
  async updateActivity(activity) {
    const { storeId, itemId, activityTime, activityType } = activity;
    await this.updateInventoryJournal(storeId, itemId, activityTime, activityType, activity);
  }

  /**
   * Insert activity.
   * @param {string} storeId
   * @param {number} itemId
   * @param {object} activity
   */
  async insertActivity(storeId, itemId, activity) {
    const key = `${storeId}:${itemId}:transaction`;
    let docs = await this._get(key);
    if (docs == null) docs = [];
    docs.push(activity);
    await this._upsert(key, docs);
  }

  /**
   * Get activities.
   * @param {string} storeId
   * @param {number} itemId
   * @param {string} [options.activityType]
   * @param {string} [options.fromActivityTime] format: '2020-02-04T17:36:06Z'
   * @param {string} [options.toActivityTime] format: '2020-02-04T17:36:06Z'
   * @returns {object[]}
   */
  async getActivities(storeId, itemId, options) {
    let docs = await this._get(`${storeId}:${itemId}:transaction`);
    if (docs == null) docs = [];
    if (options == null) return docs;

    let filteredDocs = docs;
    if ('activityType' in options) {
      filteredDocs = filteredDocs.filter(d => d.activityType == options.activityType);
    }
    if ('fromActivityTime' in options) {
      filteredDocs = filteredDocs.filter(d => moment(d.activityTime).isSameOrAfter(moment(options.fromActivityTime)));
    }
    if ('toActivityTime' in options) {
      filteredDocs = filteredDocs.filter(d => moment(d.activityTime).isSameOrBefore(moment(options.toActivityTime)));
    }
    return filteredDocs;
  }

  /**
   * Get item.
   * @param {string} storeId
   * @param {number} itemId
   * @returns {object}
   */
  async getItem(storeId, itemId) {
    const key = `${storeId}:${itemId}:item`;
    const res = await this._get(key);
    return res;
  }
}

module.exports = RedisClient;