'use strict';
const RedisClient = require('./redis-client');

const hello = async (event) => {

  const redis = new RedisClient();
  await redis.flushall();

  const storeId = 23300;
  const itemId = 1001;
  const activityTime = '2020-02-04T17:36:06Z';
  const activityType = 1;

  // inventory
  const inventoryDoc = {
    storeId,
    itemId,
    currentInventory: 10
  }

  // insert
  await redis.updateInventory(storeId, itemId, inventoryDoc);
  console.log('inventory.insert.getInventory', await redis.getInventory(storeId, itemId));

  // update
  inventoryDoc.currentInventory = 12;
  await redis.updateInventory(storeId, itemId, inventoryDoc);
  console.log('inventory.update.getInventory', await redis.getInventory(storeId, itemId));

  // inventoryJournal
  // get&create
  console.log('inventoryJournal.get.create.getInventoryJournal', await redis.getInventoryJournal(storeId, itemId, activityTime, activityType));

  // update
  await redis.updateInventoryJournal(storeId, itemId, activityTime, activityType, {
    storeId, itemId, activityTime, activityType, message: 'hello'
  });

  await redis.insertActivity(storeId, itemId, {
    storeId: 23300,
    itemId: 1001,
    activityTime: '2020-02-04T17:36:07Z',
    activityType: 1
  });

  console.log('activities:', await redis.getActivities(storeId, itemId));
  await redis.updateActivity({
    storeId: 23300,
    itemId: 1001,
    activityTime: '2020-02-04T17:36:07Z',
    activityType: 1,
    comment: '00000000'
  })
  console.log('updated activities:', await redis.getActivities(storeId, itemId));
  console.log('get activities(should return empty):', await redis.getActivities(storeId, itemId, { activityType: 2 }));
  console.log('get activities(should return 2 objects):', await redis.getActivities(storeId, itemId, { activityType: 1 }));
  console.log('get activities(should return 1 object):', await redis.getActivities(storeId, itemId, { activityType: 1, fromActivityTime: '2020-02-04T17:36:06Z', toActivityTime: '2020-02-04T17:36:06Z' }));
  console.log('get activities(should return 1 object):', await redis.getActivities(storeId, itemId, { activityType: 1, fromActivityTime: '2020-02-04T17:36:07Z', toActivityTime: '2020-02-04T17:36:07Z' }));
  console.log('get activity(should return 1 object):', await redis.getInventoryJournal(storeId, itemId, '2020-02-04T17:36:06Z', 1));
  console.log('get activity(should return 1 object):', await redis.getInventoryJournal(storeId, itemId, '2020-02-04T17:36:07Z', 1));

  // item
  // get&create
  console.log('item:', await redis.getItem(storeId, itemId));
  return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

hello();